// Java-программа текстового редактора

import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.plaf.metal.OceanTheme;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.lang.reflect.Field;

import static javax.swing.JOptionPane.YES_OPTION;

public class Editor extends JFrame {
    // Текстовый компонент
    public JTextArea textArea;

    public JMenuItem newFileItem;

    // хранит последний текст, сохранённый редактором.
    // (маркер того, сохранён ли текущий файл)
    String lastText = "";

    // true, если пользователь отменяет изменения
    boolean cancelUnsaved() {
        int result = JOptionPane.showConfirmDialog(
            this,
            "You have unsaved changes. Drop them?",
            "Unsaved changes",
            JOptionPane.YES_NO_OPTION
        );
        return result == YES_OPTION;
    }

    // Конструктор
    public Editor() {
        // Создать рамку
        setTitle("Text editor");
        try {
            // Настройка внешнего вида
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            // Установить тему 
            MetalLookAndFeel.setCurrentTheme(new OceanTheme());
        } catch (Exception e) {
        }

        // Текстовый компонент
        textArea = new JTextArea();

        // Создать строку меню
        JMenuBar mb = new JMenuBar();

        // Создать меню для File
        JMenu fileMenu = new JMenu("File");

        // Создание пунктов меню
        newFileItem = new JMenuItem("New");
        newFileItem.setName("newFileItem");
        // Добавить слушателя действия
        newFileItem.addActionListener(e -> {
            if (!lastText.equals(textArea.getText())) {
                // если выбрана отмена изменений
                if (cancelUnsaved()) {
                    textArea.setText("");
                }
            }
        });
        fileMenu.add(newFileItem);

        JMenuItem openFileItem = new JMenuItem("Open");
        // Добавить слушателя действия
        openFileItem.addActionListener(e -> {
            // если есть изменения, и пользователь хочет их сохранить,
            // новый файл не открывать
            if (!lastText.equals(textArea.getText()) && !cancelUnsaved()) {
                return;
            }

            // Создать объект класса JFileChooser
            JFileChooser fileChooser = new JFileChooser(".");
            // показать диалоговое окно открытия файла
            int result = fileChooser.showOpenDialog(null);
            // Если пользователь выбирает файл
            if (result == JFileChooser.APPROVE_OPTION) {
                // Установить метку на путь к выбранному каталогу
                File fi = new File(fileChooser.getSelectedFile().getAbsolutePath());
                try {
                    // File reader
                    FileReader fr = new FileReader(fi);
                    // Буферный ридер
                    BufferedReader br = new BufferedReader(fr);
                    // инициализируем text
                    // StringBuilder нужно использовать вместо обычных строк, когда
                    // эти строки нужно часто складывать. Этот инструмент позволяет
                    // уменьшить объём используемой памяти.
                    StringBuilder text = new StringBuilder(br.readLine());
                    // Взять ввод из файла
                    String line;
                    while ((line = br.readLine()) != null) {
                        text.append("\n").append(line);
                    }
                    // Установить текст
                    lastText = text.toString();
                    textArea.setText(lastText);
                } catch (Exception evt) {
                    JOptionPane.showMessageDialog(this, evt.getMessage());
                }
            }
        });
        fileMenu.add(openFileItem);

        JMenuItem saveAsItem = new JMenuItem("Save");
        // Добавить слушателя действия
        saveAsItem.addActionListener(e -> {
            // Создать объект класса JFileChooser
            JFileChooser fileChooser = new JFileChooser(".");
            // Вызвать функцию ShowSaveDialog, чтобы показать диалог сохранения
            int result = fileChooser.showSaveDialog(null);
            if (result == JFileChooser.APPROVE_OPTION) {
                // Установить метку на путь к выбранному каталогу
                save(fileChooser.getSelectedFile().getAbsolutePath());
            }
        });
        fileMenu.add(saveAsItem);
        mb.add(fileMenu);
        setJMenuBar(mb);
        add(textArea);

        // Проверка несохранённых изменений при выходе
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                // если выбрана отмена изменений
                if (lastText.equals(textArea.getText()) || cancelUnsaved()) {
                    // уничтожение окна
                    setVisible(false);
                    dispose();
                }
            }
        });

        setSize(500, 500);
    }

    public void save(String fileName) {
        File fi = new File(fileName);
        try {
            // Создать файл писателя
            FileWriter wr = new FileWriter(fi, false);
            // Создать буферизованный писатель для записи
            BufferedWriter w = new BufferedWriter(wr);
            // Написать
            lastText = textArea.getText();
            w.write(lastText);
            w.flush();
            w.close();
        } catch (Exception evt) {
            JOptionPane.showMessageDialog(this, evt.getMessage());
        }
    }

    // Метод запуска
    public static void main(String[] args) {
        var editor = new Editor();
        editor.setDefaultCloseOperation(EXIT_ON_CLOSE);
        editor.setVisible(true);
    }
}
