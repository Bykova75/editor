import org.junit.Test;

import java.util.*;
import java.awt.*;
import javax.swing.*;
import java.nio.file.*;
import java.nio.charset.StandardCharsets;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class EditorTests {
    static String readFile(String path) throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, StandardCharsets.UTF_8);
    }

    @Test
    public void testSave() throws IOException {
        final String testFileName = "testFiles/testSave.txt";
        File f = new File(testFileName);
        
        final String testStr = "Test write";
        var e = new Editor();
        e.newFileItem.doClick();
        e.textArea.setText(testStr);
        e.save(testFileName);
        assertEquals(testStr, readFile(testFileName));
    }

    @Test
    public void testAppend() throws IOException {
        final String testFileName = "testFiles/testAppend.txt";
        File f = new File(testFileName);
        
        final String testStr = "Test write";
        var e = new Editor();
        e.newFileItem.doClick();
        e.textArea.setText(testStr);
        e.save(testFileName);
        e.textArea.setText(e.textArea.getText() + " and append");
        e.save(testFileName);
        assertEquals(testStr + " and append", readFile(testFileName));
    }
}
